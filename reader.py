#
# This Python module reads ands stores data sent by the Propeller 
# microcontroller to be fetched Python.
#

import serial
import io
import numpy
import threading
import time

NUM_READINGS = 512 # The number of readings to cache

class SerialPortReader(object):
    def __init__(self,port='COM5',baud=115200,delimeter=' ',newline=chr(13),new_plot=True,sio=None,ser=None):
        """
        The first few args are for creating a new serial port buffer. The sio and ser arguments allow for
        passing already opened buffers into the instance. For some reason, it took a while to start reading 
        when opening a new buffer. I'm impatient. 
        """

        # Create/use an instance of the PySerial class (self.ser) and cast in an io wrapper so Python automatically handles parsing
        if sio is not None:
            self.ser = None
            self.sio = sio
        elif ser is not None:
            self.ser = ser
            io.TextIOWrapper(io.BufferedRWPair(self.ser, self.ser), newline=newline)
        else:
            self.ser = serial.Serial(port, baud, timeout=1, interCharTimeout=1) 
            self.sio = io.TextIOWrapper(io.BufferedRWPair(self.ser, self.ser), newline=newline)

        self.delimeter = delimeter

        # boilerplate to handle running in its own thread
        self.__is_shut_down = threading.Event()
        self.__shutdown_request = False

        self.setup_readings()
        self.start_time = time.time()

    def setup_readings(self):
        self.max_raw_readings = [] 
        self.min_raw_readings = []
        self.readings = []
        self.analysis_readings = []

        self.sio.readline()
        self.sio.readline()
        # throw first reading away because we don't know if it's complete

        out = self.sio.readline().strip().split(self.delimeter)
        print out

        for x in out:
            if x != "":
                x = float(x)
                # setup data processing variables
                self.max_raw_readings.append(x) 
                self.min_raw_readings.append(x)
                
                self.readings.append(0) # setup the most-recent reading variable
                self.analysis_readings.append( numpy.ones(NUM_READINGS) ) # setup the accumulated reading variable

    def read(self):
        for i,x in enumerate(self.sio.readline().strip().split(self.delimeter)):
            x = int(x)
            # any data processing could go here
            if self.max_raw_readings[i] < x:
                self.max_raw_readings[i] = x
            if self.min_raw_readings[i] > x:
                self.min_raw_readings[i] = x
            # update the current reading
            self.readings[i] = (self.max_raw_readings[i]-float(x))/(1+self.max_raw_readings[i]-self.min_raw_readings[i])
            # slide the accumulated readings to include the new reading
            self.analysis_readings[i] = numpy.delete(numpy.append(self.analysis_readings[i],x),0)

    def run_forever(self, seconds_to_regraph=0.1):
        # boilerplate to handle running in its own thread
        self.__is_shut_down.clear()
        try:
            while not self.__shutdown_request:
                self.read()
        finally:
            self.__shutdown_request = False
            self.__is_shut_down.set()

    def shutdown(self):
        """Stops the run_forever loop.

        Blocks until the loop has finished. This must be called while
        run_forever() is running in another thread, or it will
        deadlock.
        """
        self.__shutdown_request = True
        self.__is_shut_down.wait()
        self.sio.close()