''***********************************************
''*                                             *
''*  Author: Ben Margolis                       *
''*  Copyright (c) 2013 Parallax, Inc.          *               
''*  See end of file for terms of use.          *               
''***********************************************
{{


}}
CON 
  _clkmode = xtal1 + pll16x 
  _XinFREQ = 5_000_000
  NUM_LINES = 9
  NEXT_CNT_INCREMENT = 80_000_000 / 10                       '10 Hz = 100 ms
  CNT_PER_WAVELENGTH = 80_000_000 / 200                      '200 Hz = 5 ms
  Led_First_Pin = 0
  Led_LastPin = 4
  Sensor_Output_Pin = 17
  Sensor_Control_Pin = 14
  

OBJ
  pst  : "Parallax Serial Terminal"
  lfs  : "tsl230"
  
VAR
   long value
   byte line_num, WAVELENGTH
   long next_cnt


PUB go | old
  pst.start(115200)                                          'start terminal
  lfs.Start(Sensor_Output_Pin, Sensor_Control_Pin,500,false) 'start tsl230 light to frequency sensor @ 500 Hz = 2 ms
  
  dira[Led_First_Pin..Led_LastPin] := %11111                 'set LED pins as output
  next_cnt := cnt + NEXT_CNT_INCREMENT      
  
  repeat
    if cnt >= next_cnt
      next_cnt := cnt + NEXT_CNT_INCREMENT   'loop should take a total of 100ms
       
      repeat WAVELENGTH FROM Led_First_Pin TO Led_LastPin STEP 1
        'each wavelength sample time is 15ms * 5 wavelengths = 75 ms
        waitcnt(CNT_PER_WAVELENGTH + cnt)    ' wait 5ms
        outa[WAVELENGTH] := 1                ' turn on LED
        waitcnt(CNT_PER_WAVELENGTH + cnt)    ' wait 5ms 
        value := lfs.GetSample               ' pull sample from sensor
        pst.Dec(value)                       ' output sample to terminal
        waitcnt(CNT_PER_WAVELENGTH + cnt)    ' wait 5ms
        outa[WAVELENGTH] := 0                ' turn off LED
        pst.Char(" ")                        ' output terminal delimeter
        
     pst.Newline                             ' output delimter


{{

┌──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
│                                                   TERMS OF USE: MIT License                                                  │                                                            
├──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┤
│Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    │ 
│files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    │
│modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software│
│is furnished to do so, subject to the following conditions:                                                                   │
│                                                                                                                              │
│The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.│
│                                                                                                                              │
│THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          │
│WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         │
│COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   │
│ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         │
└──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
}}