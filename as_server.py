#
# This Python module creates a threaded TCP server that returns data collected over
# the serial port. The headers allow cross-domain acces so the server can be run
# without serving the actual HTML/JS to fetch and render the code
#

import SocketServer
import BaseHTTPServer
import threading

import time
from reader import SerialPortReader

class TCPHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    SerialPortReader = None
    do_log = False

    def log_message(self, *args, **kwargs):
        if self.do_log:
            return super(TCPHandler,self).log_message(*args,**kwargs)

    def do_GET(self):
        if self.SerialPortReader is None:
            raise Exception
        self.send_response(200, "OK")

        # Send access-control headers so the JS script can GET data off 
        self.send_header("Access-Control-Allow-Origin", "*")
        self.send_header("Access-Control-Allow-Headers", "x-requested-with")
        self.send_header("Content-Type", "application/json")
        self.end_headers()


        self.wfile.write( str( [acc_reading.tolist() for acc_reading in self.SerialPortReader.analysis_readings ] ) ) # send all the accumulated data
        # self.wfile.write( str( self.SerialPortReader.readings ) ) # send the latest reading

class ThreadedTCPServer(SocketServer.ThreadingMixIn, BaseHTTPServer.HTTPServer):
    pass

if __name__ == "__main__":
    # Create the server and serial port reader in separate threads
    print "Opening serial port..."
    TCPHandler.SerialPortReader = SerialPortReader()
    print "Done opening serial port"

    HOST, PORT = "localhost", 9999    
    server = ThreadedTCPServer((HOST,PORT),TCPHandler)
    server_thread = threading.Thread(target=server.serve_forever)
    server_thread.daemon = False
    server_thread.start()

    serial_thread = threading.Thread(target=TCPHandler.SerialPortReader.run_forever)
    serial_thread.daemon = False
    serial_thread.start()

    try:
        while threading.active_count() > 0:
            time.sleep(0.1)
    except:
        server.shutdown()
        TCPHandler.SerialPortReader.shutdown()
        raise


